from emoji import emoji_list
import re
import json
emoji_broken_chunks = emoji_list.split("\n")

emoji_split = [
    re.sub('\s+',' ', emoji_data).split(" ") for emoji_data in emoji_broken_chunks
]

emoji_data = { }
for emoji_unfiltered in emoji_split:
    # emoji_data[     ' '.join(    emoji_unfiltered[1:len(emoji_unfiltered)]   ).split(";")[0]       ] = re.sub( '\([A-Za-z])\w+' , '' , emoji_unfiltered[0] )
    emoji_data[     ' '.join(    emoji_unfiltered[1:len(emoji_unfiltered)]   ).split(";")[0]       ] = re.sub(
            r'([A-Za-z])\w+', '', emoji_unfiltered[0])

emoji_file = open("emoji_filtered.json", "w+")


emoji_data.pop('')

emoji_file.write(str(emoji_data))


