from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import math
import textwrap
from subprocess import Popen, PIPE
from datetime import date
import sys

today = date.today()

img_width = 0
img_height = 0


def percent_from_pos(context_number, total_size):
    return (context_number/100) * total_size


def load_font(font_family, font_size=20, custom_size=False):
    global img_height

    if not custom_size:
        if img_height > 1080: font_size = 44

    print("FONT SIZE: ", font_size, img_height)

    font = ImageFont.truetype(r'{}'.format(font_family), font_size)
    return font

def place_calender(img, draw, text, text_color):
    global img_height
    global img_width

    font = load_font('./assets/Source_Code_Pro/SourceCodePro-Black.ttf')

    w, h = draw.textsize(text, font=font)

    actual_height = (img_height - h ) / 2
    actual_height = actual_height - percent_from_pos(27, img_height)

    actual_width = (img_width - w ) - 100

    draw.text( ( actual_width, actual_height), text, font=font)


def current_date(img, draw, text, text_color):
    global img_height
    global img_width
    font = load_font('./assets/Source_Code_Pro/SourceCodePro-Black.ttf')

    w, h = draw.textsize(text, font=font)

    actual_height = (img_height - h ) / 2
    actual_height = actual_height - percent_from_pos(40, img_height)

    actual_width = (img_width - w ) - 100

    draw.text( ( actual_width, actual_height), text, font=font)

    actual_height_2 = ((img_height - h ) / 2) - percent_from_pos(39, img_height)
    draw.text( ( actual_width, actual_height_2 ), (" " * 9 + "." * (len(text) - 9)), font=font)

def place_quote(img, draw, text, text_color):
    global img_height
    global img_width
    font = load_font('./assets/Pacifico/Pacifico-Regular.ttf', 40, True)

    w, h = draw.textsize(text, font=font)

    actual_height = (img_height - h )
    actual_height = actual_height - percent_from_pos(10, img_height)

    actual_width = ( ( img_width - w ) / 2 )

    draw.text( ( actual_width, actual_height), text, font=font)


def main():
    global img_width
    global img_height

    process = Popen(["cal"], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()

    img = Image.open("./assets/main/{}".format(sys.argv[1]))
    img_width, img_height = img.size

    draw = ImageDraw.Draw(img)

    # placing current date

    today = date.today()
    current_date(img, draw, today.strftime("Today is %b %d"), (0,0,0) )



    # placing calender

    output_line = output.decode().split("\n")
    stripped_output = '\n\n'.join(output_line[1:2])
    stripped_output += '\n\n'
    stripped_output += '\n'.join(output_line[2:])
    place_calender(img, draw, stripped_output, (0,0,0))

    # to place things at the bottom then it will lower number in percent

    place_quote(img, draw, "See you in Space Cowboy ...", (0,0,0))


    img.save('sample-{}'.format(sys.argv[1]))


main()

